$(document).ready(function() {
  $('.about-slider').owlCarousel({
    loop: false,
    responsiveClass: true,
    dots: true,
    nav: false,
    responsive: {
      0: {
        items: 1,
        margin: 30,
        stagePadding: 30,
      },
      768: {
        items: 3,
        margin: 10,
        stagePadding: 10,
      },
      960: {
        items: 3,
        margin: 10,
        stagePadding: 10,
      },
      1280: {
        items: 3,
        margin: 60,
        stagePadding: 60,
        nav: true      }
    }
  });

  $('.reviews-wrap').owlCarousel({
    loop: false,
    responsiveClass: true,
    dots: true,
    nav: false,
    responsive: {
      0: {
        items: 1,
        margin: 30,
        stagePadding: 30,
      },
      768: {
        items: 1,
        margin: 10,
        stagePadding: 10,
      },
      1280: {
        items: 1,
        margin: 40,
        stagePadding: 40,
      }
    }
  });
});

// Click to toggle the hamburger-icon animation and add the fullscreen-menu overlay.
$(document).ready(function(){
  $('#nav-icon').click(function(){
    $(this).toggleClass('animate-icon');
    $('#overlay').fadeToggle();
  });
});

// Clicking anywhere on the overlay closes the fullscreen-menu overlay and resets hamburger-icon.
$(document).ready(function(){
  $('#overlay').click(function(){
    $('#nav-icon').removeClass('animate-icon');
    $('#overlay').toggle();
  });
});